import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Utama extends StatelessWidget {
  const Utama({super.key});
  
void logOut() async{
  await FirebaseAuth.instance.signOut();
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
      children: [
        SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/bg_jtravel.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    "assets/main.png",
                    width: 900.0,
                    height: 900.0,
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
          top: 70.0,
          right: 30.0,
          child: ElevatedButton.icon(
            style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.red),
            ),
            onPressed: logOut,
            icon: Icon(Icons.logout_outlined),
            label: Text("Logout"),
          ),
        ),
      ],
    ),
  );
}
} 
