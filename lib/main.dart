import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:login_uts/home.dart';
//import 'package:login_uts/profile_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: homepage(),
    );
  }
}
class homepage extends StatefulWidget {
  const homepage({super.key});

  @override
  State<homepage> createState() => _homepageState();
}

class _homepageState extends State<homepage> {
  //Initialize firebase app
  Future<FirebaseApp> _initializeFirebase() async{
    FirebaseApp firebaseApp = await Firebase.initializeApp();
    return firebaseApp;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _initializeFirebase(),
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.done){
            return loginapp();
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}


class loginapp extends StatefulWidget {
  const loginapp({super.key});

  @override
  State<loginapp> createState() => _loginappState();
}

class _loginappState extends State<loginapp> {

  //Login function
  static Future<User?> loginUsingEmailPassword({required String email, required String password, required BuildContext context}) async{
    FirebaseAuth auth = FirebaseAuth.instance;
    User? user;
    try{
      UserCredential userCredential = await auth.signInWithEmailAndPassword(email: email, password: password);
      user = userCredential.user;
    } on FirebaseAuthException catch(e){
      if (e.code == "user-not-found"){
        print("no user found for that email");
      }
    }

    return user;
  }

  @override
  Widget build(BuildContext context) {
    //create textfield controller
    TextEditingController _emailController = TextEditingController();
    TextEditingController _passwordController = TextEditingController();
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("J-Travel", 
          style: TextStyle(
            color: Colors.black, 
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
            ),
          ),
          const Text("Masuk Sekarang", 
            style: TextStyle(color: Colors.black,
            fontSize: 44.0, 
            fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 44.0,
          ),
           TextField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            decoration: const InputDecoration(
              hintText: "masukkan email",
              prefixIcon: Icon(Icons.mail,color: Colors.black,
              ),
            ),
          ),
          const SizedBox(
            height: 26.0,
          ),
           TextField(
            controller: _passwordController,
            obscureText: true,
            decoration: const InputDecoration(
              hintText: "masukkan password",
              prefixIcon: Icon(Icons.lock,color: Colors.black,
              ),
             ),
          ),
          const SizedBox(
            height: 12.0,
            ),
          const Text(
            "Lupa password?", 
            style: TextStyle(
              color: Colors.blue,
              ),
          ),
          const SizedBox(
            height: 39.0,
            ),
            Container(
              width: double.infinity,
              child: RawMaterialButton(
                fillColor: const Color(0xFF0069FE),
                elevation: 0.0,
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
                onPressed: () async{
                  User? user = await loginUsingEmailPassword(email: _emailController.text, password: _passwordController.text, context: context);
                  print(user);
                  if (user !=null){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=> Utama()));
                  }
                }, 
                child: const Text("Login", 
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      ),
                    ),
                ),
            ),
        ],
      )
    );
  }
}